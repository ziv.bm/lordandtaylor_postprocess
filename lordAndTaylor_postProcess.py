import paramiko
import os
from operator import itemgetter

# Connect to SFTP server
transport = paramiko.Transport(('uploads.wearesyte.com', 2323))
transport.connect(username='lordandtylor', password='ZTNiZTNhZW')
sftp = paramiko.SFTPClient.from_transport(transport)

# Get a list of all the files in the directory
directory = '/Outbound/manually-uploaded-deeptags-report/deeptags-report/'
files = sftp.listdir(directory)

# Get the metadata for each file and sort them based on the last modified time
files = [(f, sftp.stat(os.path.join(directory, f)).st_mtime) for f in files]
files = sorted(files, key=itemgetter(1), reverse=True)

# Get the latest file name
latest_file = files[0][0]
print(latest_file)

# Download the latest file
sftp.get(os.path.join(directory,latest_file), '/temp/' + latest_file)

# Replace the text in the 'deeptags' column
with open('/temp/' + latest_file, 'r') as f:
    lines = f.readlines()
with open('/temp/' + latest_file, 'w') as f:
    for line in lines:
        f.write(line.replace('custom_categories_original_data: ', 'category:'))

# Place the file back on the server
sftp.put('/temp/' + latest_file, os.path.join(directory,latest_file))

# Close the SFTP connection
sftp.close()
transport.close()